package kw.welcomelib;

public interface IWelcomeService {
	String DEFAULT_WELCOME_PHRASE = "Thanks for stopping by ";

	/**
	 * prepares and returns welcome message <h1> Welcome</h1>
	 * @param name Name of person to welcome
	 * @return
	 **/
	String getWelcomeMessage(String name);
}
