package kw.welcomeServive;

import kw.welcomelib.IWelcomeService;

public class WelcomeService implements IWelcomeService{

	public String getWelcomeMessage(String name) {
		
		return IWelcomeService.DEFAULT_WELCOME_PHRASE+name;
	}
	
	
}
