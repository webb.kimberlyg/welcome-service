package kw.welcomeapp.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import kw.welcomeServive.WelcomeService;
import kw.welcomelib.IWelcomeService;

public class WelcomeServiceTest {
	@Test
	public void shouldeurnWelcomeMessage() {
		IWelcomeService service= new WelcomeService();
		String name="";
		String message = service.getWelcomeMessage(name);
		assertEquals(IWelcomeService.DEFAULT_WELCOME_PHRASE +name,message);
	}
}
